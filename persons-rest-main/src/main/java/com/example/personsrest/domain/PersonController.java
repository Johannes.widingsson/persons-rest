package com.example.personsrest.domain;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequestMapping( "/api/persons/")
@RestController
@AllArgsConstructor
public class PersonController {

    PersonService personService;

   @GetMapping
    public List<PersonImpl> all(){
       return personService.all().map(PersonController :: toDTO).collect(Collectors.toList());

   }

   private static PersonImpl toDTO(PersonEntity personEntity ){
       return new PersonImpl(
               personEntity.getId(),
               personEntity.getName(),
               personEntity.getAge(),
               personEntity.getCity(),
               true,
               personEntity.getGroups()
       );
   }
}
